package facteur

import (
	"fmt"
	"github.com/stretchr/testify/assert"
	"testing"
)

func TestNewEventFromRaw_with_failing_raw(t *testing.T) {
	raw := "A|F|1|2"
	_, err := NewEventFromRaw(raw)

	assert.Error(t, err)
}

func TestNewEventFromRaw_with_valid_raw(t *testing.T) {
	raw := "1|F|1|2"
	_, err := NewEventFromRaw(raw)

	assert.NoError(t, err)
}

func TestFromRaw_with_invalid_seq(t *testing.T) {
	ev := &Event{}
	raw := "A|F|1|2"
	err := ev.FromRaw(raw)

	assert.Error(t, err)
}

func TestFromRaw_with_two_parts_message(t *testing.T) {
	ev := &Event{}
	raw := "1|B"
	err := ev.FromRaw(raw)

	assert.NoError(t, err)
	assert.Equal(t, ev.Seq, 1)
	assert.Equal(t, ev.Action, BROADCAST_ACTION)
}

func TestFromRaw_with_four_parts_message(t *testing.T) {
	ev := &Event{}
	raw := "1|F|1|2"
	err := ev.FromRaw(raw)

	assert.NoError(t, err)
	assert.Equal(t, ev.Seq, 1)
	assert.Equal(t, ev.Action, FOLLOW_ACTION)
	assert.Equal(t, ev.FromId, "1")
	assert.Equal(t, ev.ToId, "2")
}

func TestPushEventsToQueue(t *testing.T) {
	events_handler := NewEventsHandler()
	assert.Equal(t, events_handler.Queue.Len(), 0)

	first_ev := "1|B"
	second_ev := "2|B"

	// Ensure events are pushed to eventshandler queue
	events_handler.PushEventsToQueue([]string{first_ev, second_ev})
	assert.Equal(t, events_handler.Queue.Len(), 2)
	assert.Equal(t, events_handler.Queue.Head().priority, 1)

	// Ensure EventsHandler channel emits back
	// the event in order
	ev := <-events_handler.EventsChannel
	assert.Equal(t, ev.Seq, 1)

	ev = <-events_handler.EventsChannel
	assert.Equal(t, ev.Seq, 2)
}

func TestIncompleteEventMessage_with_zero_terminated(t *testing.T) {
	buffer := make([]byte, 1024)
	copy(buffer, "1|F|1|2")

	assert.False(t, incompleteEventMessage(buffer))
}

func TestIncompleteEventMessage_with_delimiter_ended(t *testing.T) {
	buffer := []byte(fmt.Sprintf("1|F|1|2%s", MSG_DELIMITER))

	assert.False(t, incompleteEventMessage(buffer))
}

func TestIncompleteEventMessage(t *testing.T) {
	buffer := []byte(fmt.Sprintf("1|F|1|2%s2|B", MSG_DELIMITER))

	assert.True(t, incompleteEventMessage(buffer))
}

func TestBackslashEndedEventMessage_with_delimited_data(t *testing.T) {
	data := []byte(fmt.Sprintf("1|F|1|2%s", MSG_DELIMITER))

	assert.False(t, incompleteEventMessage(data))
}

func TestBackslashEndedEventMessage(t *testing.T) {
	data := []byte("1|F|1|2")

	assert.True(t, incompleteEventMessage(data))
}
