package facteur

import (
	"errors"
	"fmt"
	"io/ioutil"
	"log"
	"os"
	"os/signal"
	"path/filepath"
	"strconv"
	"syscall"
)

func createPidFile(pidfile string) error {
	// Ensure pidfile base dir exists and is writable
	dir := filepath.Dir(pidfile)
	_, err := os.Stat(dir)
	if err != nil {
		return errors.New(fmt.Sprintf("[ERROR] Facteur can't open pidfile directory for writing. Reason: '%s'", err))
	}

	if pidString, err := ioutil.ReadFile(pidfile); err == nil {
		pid, err := strconv.Atoi(string(pidString))
		if err == nil {
			if _, err := os.Stat(fmt.Sprintf("/proc/%d/", pid)); err == nil {
				return fmt.Errorf("pid file found, ensure facteur is not running or delete %s", pidfile)
			}
		}
	}

	file, err := os.Create(pidfile)
	if err != nil {
		log.Println(err)
		return err
	}

	defer file.Close()

	_, err = fmt.Fprintf(file, "%d", os.Getpid())
	return err
}

func removePidFile(pidfile string) {
	if err := os.Remove(pidfile); err != nil {
		log.Printf("Error removing %s: %s", pidfile, err)
	}
}

// Daemon runs the facteur server and registers the current
// process pid into a file to keep track of it. Listens
// for SIGTERM system signal and eventually shuts down
// facteur when it's received.
func Daemon(server *Server, config *Config) error {
	if err := createPidFile(config.PidFile); err != nil {
		log.Fatal(err)
	}
	defer removePidFile(config.PidFile)

	c := make(chan os.Signal, 1)
	signal.Notify(c, os.Interrupt, os.Kill, os.Signal(syscall.SIGTERM))

	go func() {
		sig := <-c
		log.Printf("Received signal '%v', exiting\n", sig)
		removePidFile(config.PidFile)
		os.Exit(0)
	}()

	return server.Run()
}
