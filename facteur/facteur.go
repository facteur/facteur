package main

import (
	facteur "bitbucket.com/facteur/facteur"
	l4g "github.com/alecthomas/log4go"
	"log"
)

func main() {
	var err error

	l4g.Info("Facteur started")

	// Parse command line arguments
	cmdline := &facteur.Cmdline{}
	cmdline.ParseArgs()

	// Load configuration from file
	config := facteur.NewConfig()
	err = config.FromFile(*cmdline.ConfigFile, "core")
	if err != nil {
		log.Printf("[INFO] Configuration file was not loaded. Reaseon: %s", err)
	}
	config.UpdateFromCmdline(cmdline)

	// Set up loggers
	l4g.AddFilter("stdout", facteur.LogLevels[config.LogLevel], l4g.NewConsoleLogWriter())
	err = facteur.SetupFileLogger("file", config.LogLevel, config.LogFile)
	if err != nil {
		log.Fatal(err)
	}

	// Build clients store service
	clients_store := facteur.NewClientStore()
	clients_store.InitSocket(*cmdline.Host, *cmdline.ClientsPort)

	// Build events flow handling service
	events_handler := facteur.NewEventsHandler()
	events_handler.InitSocket(*cmdline.Host, *cmdline.EventsPort)

	// Build the events forwarder service
	forwarder := facteur.NewForwarder(clients_store, events_handler)

	// Start the to listen, acknowledge and serve
	server := facteur.NewServer(clients_store, events_handler, forwarder)

	if config.Daemon {
		if err := facteur.Daemon(server, config); err != nil {
			log.Fatal(err)
		}
	} else {
		server.Run()
	}
}
