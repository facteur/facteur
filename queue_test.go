package facteur

import (
	"container/heap"
	"github.com/stretchr/testify/assert"
	"testing"
)

func TestHead_returns_lowest_item(t *testing.T) {
	pq := &PriorityQueue{}
	heap.Init(pq)
	assert.Equal(t, pq.Len(), 0)

	higher_item := &Item{
		value:    &Event{},
		priority: 2,
	}
	lower_item := &Item{
		value:    &Event{},
		priority: 1,
	}

	heap.Push(pq, higher_item)
	assert.Equal(t, pq.Len(), 1)

	heap.Push(pq, lower_item)
	assert.Equal(t, pq.Len(), 2)

	assert.Equal(t, pq.Head().priority, 1)

}

func TestPush_protects_min_order(t *testing.T) {
	pq := &PriorityQueue{}
	heap.Init(pq)
	assert.Equal(t, pq.Len(), 0)

	higher_item := &Item{
		value:    &Event{},
		priority: 2,
	}
	lower_item := &Item{
		value:    &Event{},
		priority: 1,
	}

	heap.Push(pq, higher_item)
	assert.Equal(t, pq.Len(), 1)

	heap.Push(pq, lower_item)
	assert.Equal(t, pq.Len(), 2)

	// Ensure that lower priority item is popped out
	// first
	assert.Equal(t, heap.Pop(pq).(*Item).priority, 1)
	assert.Equal(t, heap.Pop(pq).(*Item).priority, 2)
}

func TestPop_protects_min_order(t *testing.T) {
	pq := &PriorityQueue{}
	heap.Init(pq)

	higher_item := &Item{
		value:    &Event{},
		priority: 2,
	}
	lower_item := &Item{
		value:    &Event{},
		priority: 1,
	}

	heap.Push(pq, lower_item)
	heap.Push(pq, higher_item)

	assert.Equal(t, heap.Pop(pq).(*Item).priority, 1)
}
