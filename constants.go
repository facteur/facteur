package facteur

// Sockets buffer sizes
const (
	EVENTS_FLOW_BUF_SIZE = 4096
	CLIENTS_REG_BUF_SIZE = 256
)

// Messages constants
const (
	MSG_DELIMITER          = "\n"
	EVENT_PARAMS_SEPARATOR = '|'
)

// Actions tokens
const (
	FOLLOW_ACTION          = "F"
	UNFOLLOW_ACTION        = "U"
	BROADCAST_ACTION       = "B"
	PRIVATE_MESSAGE_ACTION = "P"
	STATUS_UPDATE_ACTION   = "S"
)

// Timeouts in seconds
const (
	CLIENT_REG_CONN_TIMEOUT = 1
	CLIENT_REG_ID_TIMEOUT   = 30
	EVENT_REG_CONN_TIMEOUT  = 1
	EVENT_FLOW_TIMEOUT      = 30
)

// Configuration fallback constants
const (
	DEFAULT_CONFIG_FILE  = "/etc/facteur/facteur.conf"
	DEFAULT_LOG_FILE     = "/tmp/facteur.log"
	DEFAULT_PID_FILE     = "/tmp/facteur.pid"
	DEFAULT_LOG_LEVEL    = "INFO"
	DEFAULT_DAEMON_MODE  = false
	DEFAULT_HOST         = "localhost"
	DEFAULT_CLIENTS_PORT = ":9099"
	DEFAULT_EVENTS_PORT  = ":9090"
)
