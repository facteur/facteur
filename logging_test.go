package facteur

import (
	"github.com/stretchr/testify/assert"
	"os"
	"testing"
)

func TestSetupFileLogger_with_non_existing_parent_dir(t *testing.T) {
	err := SetupFileLogger("test_logger", "DEBUG", "/non/existent/dir/test.log")
	assert.Error(t, err)
}

func TestSetupFileLogger_with_unsufficient_permissions(t *testing.T) {
	err := SetupFileLogger("test_logger", "DEBUG", "/root/test.log")
	assert.Error(t, err)
	defer os.Remove("/root/test.log")
}

func TestSetupFileLogger(t *testing.T) {
	err := SetupFileLogger("test_logger", "DEBUG", "/tmp/test.log")
	assert.NoError(t, err)
	defer os.Remove("/tmp/test.log")
}
