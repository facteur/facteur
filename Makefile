FACTEUR_PACKAGE := bitbucket.com/facteur/facteur

BUILD_DIR := $(CURDIR)/.gopath

GOPATH ?= $(BUILD_DIR)
export GOPATH

GO_OPTIONS ?=
ifeq ($(VERBOSE), 1)
GO_OPTIONS += -v
endif

GIT_COMMIT = $(shell git rev-parse --short HEAD)
GIT_STATUS = $(shell test -n "`git status --porcelain`" && echo "+CHANGES")

NO_MEMORY_LIMIT ?= 0
export NO_MEMORY_LIMIT

# Set Go executable to use two threads
GOMAXPROCS ?= 2
export GOMAXPROCS


BUILD_OPTIONS = -ldflags "-X main.GIT_COMMIT $(GIT_COMMIT)$(GIT_STATUS) -X main.NO_MEMORY_LIMIT $(NO_MEMORY_LIMIT)"

SRC_DIR := $(GOPATH)/src

# Facteur specific directives
FACTEUR_DIR := $(SRC_DIR)/$(FACTEUR_PACKAGE)
FACTEUR_MAIN := $(FACTEUR_DIR)/facteur

FACTEUR_BIN_RELATIVE := bin/facteur
FACTEUR_BIN := $(CURDIR)/$(FACTEUR_BIN_RELATIVE)

.PHONY: all clean test

all: $(FACTEUR_BIN)

$(FACTEUR_BIN): $(FACTEUR_DIR)
	@(mkdir -p  $(dir $@))
	@(cd $(FACTEUR_MAIN); go get $(GO_OPTIONS); go build $(GO_OPTIONS) $(BUILD_OPTIONS) -o $@)
	@echo $(FACTEUR_BIN_RELATIVE) is created.

$(FACTEUR_DIR):
	@mkdir -p $(dir $@)
	@ln -sf $(CURDIR)/ $@

clean:
ifeq ($(GOPATH), $(BUILD_DIR))
	@rm -rf $(BUILD_DIR)
else ifneq ($(FACTEUR_DIR), $(realpath $(FACTEUR_DIR)))
	@rm -f $(FACTEUR_DIR)
endif

test: all
	@(go get "github.com/stretchr/testify/assert")
	@(cd $(FACTEUR_DIR); go test $(GO_OPTIONS))

fmt:
	@gofmt -s -l -w .
