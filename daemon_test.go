package facteur

import (
	"github.com/stretchr/testify/assert"
	"os"
	"testing"
)

func TestCreatePidFile_in_non_existing_dir(t *testing.T) {
	err := createPidFile("/blabla/dir/test.pid")
	assert.Error(t, err)
}

func TestCreatePidFile_without_write_rights(t *testing.T) {
	// I bet you ain't got right to write their
	err := createPidFile("/root/test.pid")
	assert.Error(t, err)
	defer os.Remove("/root/test.pid")
}

func TestCreatePidFile_with_already_existing_pid_file(t *testing.T) {
	err := createPidFile("/tmp/test.pid")
	assert.NoError(t, err)
	os.Remove("/tmp/test.pid")
}
