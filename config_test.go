package facteur

import (
	goconfig "github.com/msbranco/goconfig"
	"github.com/stretchr/testify/assert"
	"os"
	"testing"
)

func TestNewConfig_is_initialized_with_constants(t *testing.T) {
	config := NewConfig()

	assert.Equal(t, config.Daemon, DEFAULT_DAEMON_MODE)
	assert.Equal(t, config.LogLevel, DEFAULT_LOG_LEVEL)
	assert.Equal(t, config.LogFile, DEFAULT_LOG_FILE)
	assert.Equal(t, config.PidFile, DEFAULT_PID_FILE)
}

func TestLoadConfigFromFile(t *testing.T) {
	config := NewConfig()

	// Add a base section and options and write it
	// to a temporary conf file
	c := goconfig.NewConfigFile()
	c.AddSection("core")
	c.AddOption("core", "daemonize", "false")
	c.AddOption("core", "log_file", "/var/log/facteur/facteur_test.log")
	c.AddOption("core", "log_level", "INFO")
	c.AddOption("core", "pidfile", "/var/run/facteur/facteur.pid")
	c.WriteConfigFile("/tmp/test.conf", 0644, "Temporary facteur config")
	defer os.Remove("/tmp/test.conf")

	err := loadConfigFromFile("/tmp/test.conf", config, "core")
	assert.NoError(t, err)

	assert.Equal(t, config.Daemon, false)
	assert.Equal(t, config.LogFile, "/var/log/facteur/facteur_test.log")
	assert.Equal(t, config.LogLevel, "INFO")
	assert.Equal(t, config.PidFile, "/var/run/facteur/facteur.pid")
}

func TestLoadConfigFromFile_non_existing_file(t *testing.T) {
	config := NewConfig()
	err := loadConfigFromFile("/non/existent/test_file.conf", config, "core")
	assert.Error(t, err)
}
