package facteur

import (
	"github.com/stretchr/testify/assert"
	"testing"
)

func TestInitClientRelationship_with_non_inited(t *testing.T) {
	client_store := NewClientStore()
	events_handler := NewEventsHandler()
	f := NewForwarder(client_store, events_handler)

	f.initClientRelationship("1")
	assert.NotNil(t, f.ClientStore.Relationships["1"])
}

func TestInitClientRelationship_with_inited(t *testing.T) {
	client_store := NewClientStore()
	events_handler := NewEventsHandler()
	f := NewForwarder(client_store, events_handler)
	f.ClientStore.Relationships["1"] = NewRelationStore()

	f.initClientRelationship("1")
	assert.NotNil(t, f.ClientStore.Relationships["1"])
}

func TestAddFollower_with_non_inited(t *testing.T) {
	client_store := NewClientStore()
	events_handler := NewEventsHandler()
	f := NewForwarder(client_store, events_handler)

	f.addFollower("1", "2")
	assert.Equal(t, f.ClientStore.Relationships["1"].Followers["2"], true)
}

func TestAddFollower_with_inited(t *testing.T) {
	client_store := NewClientStore()
	events_handler := NewEventsHandler()
	f := NewForwarder(client_store, events_handler)

	f.initClientRelationship("1")
	f.addFollower("1", "2")
	assert.Equal(t, f.ClientStore.Relationships["1"].Followers["2"], true)
}

func TestAddFollowing_with_non_inited(t *testing.T) {
	client_store := NewClientStore()
	events_handler := NewEventsHandler()
	f := NewForwarder(client_store, events_handler)

	f.addFollowing("1", "2")
	assert.Equal(t, f.ClientStore.Relationships["1"].Following["2"], true)
}

func TestAddFollowing_with_inited(t *testing.T) {
	client_store := NewClientStore()
	events_handler := NewEventsHandler()
	f := NewForwarder(client_store, events_handler)

	f.initClientRelationship("1")
	f.addFollowing("1", "2")
	assert.Equal(t, f.ClientStore.Relationships["1"].Following["2"], true)
}

func TestUnfollow(t *testing.T) {
	client_store := NewClientStore()
	events_handler := NewEventsHandler()
	f := NewForwarder(client_store, events_handler)

	f.addFollower("1", "2")
	f.initClientRelationship("2")
	f.ClientStore.Relationships["2"].Followers["1"] = true
	f.Unfollow(&Event{Seq: 1, Action: UNFOLLOW_ACTION, FromId: "1", ToId: "2"})

	_, following_ok := f.ClientStore.Relationships["1"].Following["2"]
	// _, follower_ok := f.ClientStore.Relationships["2"].Followers["1"]

	assert.False(t, following_ok)
	// assert.False(t, follower_ok)
}
