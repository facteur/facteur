package facteur

import (
	goconfig "github.com/msbranco/goconfig"
	"reflect"
)

type Config struct {
	Daemon   bool   `ini:"daemonize"`
	LogFile  string `ini:"log_file"`
	LogLevel string `ini:"log_level"`
	PidFile  string `ini:"pidfile"`
}

func NewConfig() *Config {
	return &Config{
		Daemon:   DEFAULT_DAEMON_MODE,
		LogLevel: DEFAULT_LOG_LEVEL,
		LogFile:  DEFAULT_LOG_FILE,
		PidFile:  DEFAULT_PID_FILE,
	}
}

func (c *Config) FromFile(path string, section string) error {
	return loadConfigFromFile(path, c, section)
}

func loadConfigFromFile(path string, obj interface{}, section string) error {
	ini_config, err := goconfig.ReadConfigFile(path)
	if err != nil {
		return err
	}

	config := reflect.ValueOf(obj).Elem()
	config_type := config.Type()

	for i := 0; i < config.NumField(); i++ {
		struct_field := config.Field(i)
		field_tag := config_type.Field(i).Tag.Get("ini")

		switch {
		case struct_field.Type().Kind() == reflect.Bool:
			config_value, err := ini_config.GetBool(section, field_tag)
			if err == nil {
				struct_field.SetBool(config_value)
			}
		case struct_field.Type().Kind() == reflect.String:
			config_value, err := ini_config.GetString(section, field_tag)
			if err == nil {
				struct_field.SetString(config_value)
			}
		case struct_field.Type().Kind() == reflect.Int:
			config_value, err := ini_config.GetInt64(section, field_tag)
			if err == nil {
				struct_field.SetInt(config_value)
			}
		}
	}

	return nil
}

// A bit verbose, and not that dry, but could not find
// more clever for now.
func (c *Config) UpdateFromCmdline(cmdline *Cmdline) {
	if *cmdline.DaemonMode != DEFAULT_DAEMON_MODE {
		c.Daemon = *cmdline.DaemonMode
	}

	if *cmdline.PidFile != DEFAULT_PID_FILE {
		c.PidFile = *cmdline.PidFile
	}

	if *cmdline.LogFile != DEFAULT_LOG_FILE {
		c.LogFile = *cmdline.LogFile
	}

	if *cmdline.LogLevel != DEFAULT_LOG_LEVEL {
		c.LogLevel = *cmdline.LogLevel
	}
}
