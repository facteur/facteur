package facteur

import (
	"container/heap"
	l4g "github.com/alecthomas/log4go"
	"time"
)

// Forwarder is the struct responsible to forward events to
// their actual destination according to their content.
// To do so it is built upon the Service interface (in order
// to be ran in a long running goroutine easily). It has
// a pointer to a ClientStore which it will use to contact
// the destination clients, a pointer to an EventsHandler
// to trigger actions and de-queue events on new events receive,
// and finally a NextSeqId to keep track of the id of the last event
// forwarded (to protect send order)
type Forwarder struct {
	Service
	ClientStore   *ClientStore
	EventsHandler *EventsHandler
	NextSeqId     int
}

// NewForwarder builds a new Forwarder instance. Initializes
// NextSeqId to 1.
func NewForwarder(store *ClientStore, events_handler *EventsHandler) *Forwarder {
	return &Forwarder{
		Service:       *NewService("Forwarder"),
		ClientStore:   store,
		EventsHandler: events_handler,
		NextSeqId:     1,
	}
}

// Run launches a ProcessEvents long running go routine, and
// calls Handle when it sends new events to be actually sent
// on it's channel.
func (f *Forwarder) Run() {
	defer f.waitGroup.Done()

	// Launch a ProcessEvents long running go routine
	// to watch the EventsHandler queue and eventually
	// trigger Handle when new events are ready.
	// Whenever a new event is ready to be handled, ProcessEvents
	// will send it through ready_events channel.
	processor_state := make(chan bool)
	ready_events := make(chan *Event)
	f.waitGroup.Add(1)
	go f.ProcessEvents(processor_state, ready_events)

	for {
		select {
		case <-f.ch:
			close(processor_state)
			return
		case event := <-ready_events:
			f.Handle(event)
		}
	}
}

// ProcessEvents watches the top of Forwarder's EventsHandler queue.
// Whenever an event with Seq == NextSeqId and as long as this condition is
// true, ProcessEvents will pop that event from EventsHandler queue and send
// it into the ready_events channel for it to be handled.
func (f *Forwarder) ProcessEvents(state chan bool, ready_events chan *Event) {
	defer f.waitGroup.Done()

	for {
		select {
		case <-state:
			return
		case <-f.EventsHandler.EventsChannel:
			// Lookup the queue's head if there's content in the queue
			if f.EventsHandler.Queue.Len() > 0 {
				queue_head := f.EventsHandler.Queue.Head()

				for queue_head != nil && queue_head.value.Seq == f.NextSeqId {
					event := heap.Pop(f.EventsHandler.Queue).(*Item).value
					f.NextSeqId += 1

					l4g.Logf(l4g.DEBUG, "[%s.ProcessEvents] Forwarding %s", f.name, event.raw)
					ready_events <- event

					// as queue's head was popped, set the queue_head
					// iteration condition to new head
					queue_head = f.EventsHandler.Queue.Head()
				}
			}
		}
	}
}

// initClientRelationship makes sure the Forwarder's ClientStore
// Relationships store for two client ids has been initialized
func (f *Forwarder) initClientRelationship(clientId string) {
	// Taking clientstore lock to be able to init relationship
	// store safely
	f.ClientStore.Lock()

	// Init clientId relations store in clients store if it doesn't exist
	if f.ClientStore.Relationships[clientId] == nil {
		f.ClientStore.Relationships[clientId] = NewRelationStore()
	}

	f.ClientStore.Unlock()
}

// addFollower add followerId to the list of clientId followers
// in Forwarder's ClientStore relationships store.
// Nota: addFollower makes sure clientId relations store is
// initialized before any operations. Plus, the method makes
// sure to acquire clients store relationships map lock to
// ensure concurrent operations safety
func (f *Forwarder) addFollower(clientId, followerId string) {
	// Init clientId relations store in clients store
	f.initClientRelationship(clientId)

	// obtain client id relation store lock and safely add
	// the new follower
	f.ClientStore.Relationships[clientId].Lock()
	f.ClientStore.Relationships[clientId].Followers[followerId] = true
	f.ClientStore.Relationships[clientId].Unlock()
}

// addFollowing add followingId to the list of clientId followings
// in Forwarder's ClientStore relationships store.
// Nota: addFollowing makes sure clientId relations store is
// initialized before any operations. Plus, the method makes
// sure to acquire clients store relationships map lock to
// ensure concurrent operations safety
func (f *Forwarder) addFollowing(clientId, followingId string) {
	// Init clientId relations store in clients store
	f.initClientRelationship(clientId)

	// obtain client id relation store lock and safely add the
	// new following
	f.ClientStore.Relationships[clientId].Lock()
	f.ClientStore.Relationships[clientId].Following[followingId] = true
	f.ClientStore.Relationships[clientId].Unlock()
}

// Handle triggers a Forwarder action according to the event's
// action.
func (f *Forwarder) Handle(ev *Event) {
	switch ev.Action {
	case FOLLOW_ACTION:
		f.Follow(ev)
	case UNFOLLOW_ACTION:
		f.Unfollow(ev)
	case BROADCAST_ACTION:
		f.Broadcast(ev)
	case PRIVATE_MESSAGE_ACTION:
		f.PrivateMessage(ev)
	case STATUS_UPDATE_ACTION:
		f.StatusUpdate(ev)
	}

}

// Follow sends a follow notification to the destination client,
// registers destination as source following, and source as destination
// follower.
// If destination client is not connected an error will be logged
// and follow will fail silently
func (f *Forwarder) Follow(ev *Event) {
	// Add source client as destination follower, and destination client
	// as source following.
	// Nota: locks the ClientStore
	f.addFollower(ev.ToId, ev.FromId)
	f.addFollowing(ev.FromId, ev.ToId)

	// Obtain ClientStore read-lock
	f.ClientStore.RLock()

	if client, ok := f.ClientStore.Container[ev.ToId]; ok {
		// Forward event to destination or fail silently
		f.ClientStore.Container[ev.ToId].SetDeadline(time.Now().Add(time.Duration(30) * time.Second))
		_, err := client.Write([]byte(ev.raw))
		if err != nil {
			l4g.Logf(l4g.ERROR, "[%s.Follow] %s", f.name, err)
		} else {
			l4g.Logf(l4g.DEBUG, "[%s.Follow] %s sent to %s", f.name, ev.raw, ev.ToId)
		}
	} else {
		l4g.Logf(l4g.ERROR, "[%s.Follow] No active connection found for client %s", f.name, ev.ToId)
	}

	f.ClientStore.RUnlock()
}

// Unfollow removes source client from destination followers and
// destination client from source following.
// If source client was not following destination, an error will be
// logged and Unfollow will fail silently.
func (f *Forwarder) Unfollow(ev *Event) {
	// Make sure both from and to clients relationship stores
	// are initialized
	f.initClientRelationship(ev.FromId)
	f.initClientRelationship(ev.ToId)

	_, fromid_in_clientstore := f.ClientStore.Container[ev.FromId]
	_, fromid_following_toid := f.ClientStore.Relationships[ev.FromId].Following[ev.ToId]

	if !fromid_in_clientstore || !fromid_following_toid {
		l4g.Logf(l4g.ERROR, "[%s.Unfollow] Client %s can not unfollow %s", f.name, ev.FromId, ev.ToId)
	} else {
		f.ClientStore.Relationships[ev.FromId].Lock()
		f.ClientStore.Relationships[ev.ToId].Lock()

		delete(f.ClientStore.Relationships[ev.FromId].Following, ev.ToId)
		delete(f.ClientStore.Relationships[ev.ToId].Followers, ev.ToId)

		f.ClientStore.Relationships[ev.FromId].Unlock()
		f.ClientStore.Relationships[ev.ToId].Unlock()
	}
}

// Broadcast forwards the broadcast event to every connected
// clients
func (f *Forwarder) Broadcast(ev *Event) {
	f.ClientStore.RLock()

	for _, socket := range f.ClientStore.Container {
		_, err := socket.Write([]byte(ev.raw))
		if err != nil {
			continue
		}
	}

	f.ClientStore.RUnlock()
}

// PrivateMessage forwards a private message from source to
// destination client.
// If destination client is unknown or not connected, and error
// will be logged and PrivateMessage will fail silently.
func (f *Forwarder) PrivateMessage(ev *Event) {
	// Make sure both from and to clients relationship stores
	// are initialized
	// Nota: locks the ClientStore
	f.initClientRelationship(ev.FromId)
	f.initClientRelationship(ev.ToId)

	f.ClientStore.RLock()

	if _, ok := f.ClientStore.Container[ev.ToId]; !ok {
		l4g.Logf(l4g.ERROR, "[%s.Private message] unknow destination %s", f.name, ev.ToId)
	} else {
		_, err := f.ClientStore.Container[ev.ToId].Write([]byte(ev.raw))
		if err != nil {
			l4g.Logf(l4g.ERROR, "[%s.Private message] %s unreachable", f.name, ev.ToId)
		}
	}

	f.ClientStore.RUnlock()
}

// StatusUpdate forwards an event to every source client followers.
// For each unknown or non-connected clients, StatusUpdate will log
// an error and fail silently.
func (f *Forwarder) StatusUpdate(ev *Event) {
	// Make sure from and to clients relation store are initialized
	// Nota: locks the ClientStore
	f.initClientRelationship(ev.FromId)
	f.initClientRelationship(ev.ToId)

	f.ClientStore.Relationships[ev.FromId].RLock()

	for client_id := range f.ClientStore.Relationships[ev.FromId].Followers {
		if _, ok := f.ClientStore.Container[client_id]; ok {
			_, err := f.ClientStore.Container[client_id].Write([]byte(ev.raw))
			if err != nil {
				l4g.Logf(l4g.ERROR, "[Status update] Sending %s status update to %s failed", ev.FromId, client_id)
				continue
			}
		}
	}

	f.ClientStore.Relationships[ev.FromId].RUnlock()
}
