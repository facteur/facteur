# Facteur (french word for postman)


A [Go](http://golang.org/) language event machine responding to the criterias described in `subject/instructions.md`.

Facteur will listen and parse an event stream, and will forward them according to their destination to connected users. It relies on the usage of goroutines as services connected each other through channels, and makes use of a central message priority queue to make sure as few messages as possible are stored in-memory.

The core of the project relies on the Go language standard library only. But, as go language doesn't provide certain features, the following external libraries have been use to enhance the user experience and testing:

* [Log4Go](https://github.com/alecthomas/log4go) a logging facility which provides a simple interface to file logging.
* [GoConfig](https://github.com/msbranco/goconfig) a INI config files parsing/creation library
* [Testify](https://github.com/stretchr/testify) a testing assertions and mocking library to enhance the native golang testing interface


## Notes to the corrector

* Though ``subject/instructions.md`` indicates that message delimiter should be "\r\n", the provided followermaze test script (that you can find in ``subject/`` folder) sends messages separated by only "\n". Facteur took it in account, and used '\n' as a message delimiter. So please whether use the copy of ``followermaze.sh`` and jar located in ``subject/`` folder, or change the MSG_DELIMITER constant value to "\r\n" in ``constants.go`` file and recompile.

* Golang is quite young and using an up to date version of the compiler is vital. Facteur won't compile with a Go lang version ``< 1.1`` please make sure yours is above or equal.



## Installation


### Binaries

Precompiled binaries of the project for i386, x86_64 and arm architectures can found in the
project ``bin`` folder. Just copy it anywhere on your file system a go ahead with ``usage`` instructions


### Vagrant

To avoid the "it works on my machine syndrom", facteur has been developped using [Vagrant](http://www.vagrantup.com/). If you're using vagrant too, a Vagrantfile for facteur is ready to create and provision an ubuntu precise32 distribution for you.

Just move to the project's root and run ``vagrant up``

Once your vm is ready, ssh to it ``vagrant ssh`` and you're ready to go ahead with ``usage``() instructions

### Or, Build it (the old fashion way, you know...)

1. First, make sure you have a `Go <http://http://golang.org/>`_ language compiler ``>= 1.1`` (<- **mandatory**) and `git <http://gitscm.org>`_ installed.

2. Then, just build and copy the ``./bin/facteur`` executable to a system ``PATH`` location

```bash
  make
  sudo cp ./bin/facteur /usr/local/bin/facteur
```

*Nota*: as a default the executable is compiled to spread goroutines on two threads only. If you'd wanna increase the value, just pass the GOMAXPROCS value during the make : ``make GOMAXPROCS=4``

## Usage

Once Facteur is installed, you can launch the server using the facteur executable.

```bash
  $ facteur -h
  /bin/facteur -h
  Usage of ./bin/facteur:
    -clients-port=":9099": Port to be used for clients registration
    -config="/etc/facteur/facteur.conf": Specifies config file path
    -daemon=false: Launches facteur as a daemon
    -events-port=":9090": Port to be used for events registration
    -host="localhost": Sets the host to bind facteur sockets to
    -log-file="/var/log/facteur/facteur.log": Specifies in which file facteur should eventually log
    -log-level="INFO": Sets facteur verbosity
    -pid-file="/var/run/facteur/facteur.pid": Specifies which pid file facteur should maintain when in daemon mode
```

*Nota*: As a default facteur will log into /tmp/facteur.log and store it's pidfile as /tmp/facteur.pid in order not to bother with rights. However it is strongly advised to create a ``/var/log/facteur`` and ``/var/run/facteur`` directories and tell facteur to use them.


You'll probably want to use the ``-daemon`` option, which will run Facteur in daemon mode. Facteur should be run in background then, and the process will be manageable through it's pid file defined via the configuration file or command line option ``pidfile``.

To run it in daemon mode, just:

```bash
    $ facteur -daemon &
```


## Configuration


Facteur core is already pre-configured, however if you'd wanna tune things up, server configuration can be updated using an INI file you can pass as ``–config`` argument. As a default
Facteur will search for configuration file at ``/etc/facteur/facteur.conf``.

**example config (config/facteur.conf)**

```ini
  [core]
  daemonize=false
  log_file=/var/log/facteur/facteur.log
  log_level=INFO
  pidfile=/var/run/facteur/facteur.pid
```

## Tests

To run tests, from the project root, just :

```bash
    $ make test
```