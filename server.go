package facteur

import (
	l4g "github.com/alecthomas/log4go"
	"os"
	"os/signal"
	"syscall"
)

// Server implements the Service interface and exposes the Facteur
// different services.
type Server struct {
	Service

	Store        *ClientStore
	EventsSource *EventsHandler
	Forwarder    *Forwarder
}

// Server initializes a new Server instance
func NewServer(store *ClientStore, events_handler *EventsHandler, forwarder *Forwarder) *Server {
	return &Server{
		Service:      *NewService("Server"),
		Store:        store,
		EventsSource: events_handler,
		Forwarder:    forwarder,
	}
}

// Run launches the server's services and listens for SIGINT
// and SIGTERM signals to gracefully them on receive
func (s *Server) Run() error {
	defer s.waitGroup.Done()

	// Handle SIGINT and SIGTERM signals for gracefull shutdown sake.
	ch := make(chan os.Signal)
	signal.Notify(ch, syscall.SIGINT, syscall.SIGTERM)
	go func() {
		for sig := range ch {
			l4g.Logf(l4g.INFO, "[%s.Run] %s received, stopping the facteur", s.name, sig)
			s.Store.NetworkService.Stop()
			s.Forwarder.Service.Stop()
			s.EventsSource.NetworkService.Stop()
			os.Exit(1)
		}
	}()

	s.waitGroup.Add(2)
	go s.Store.Serve()
	go s.Forwarder.Run()
	s.EventsSource.Serve()
	return nil
}

func ListenAndAcknowledge() error {
	return nil
}
